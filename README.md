# docker-lab

## References
- [Containers from Scratch by Liz Rice - explains exactly what a container is](https://www.youtube.com/watch?v=8fi7uSYlOdc&feature=youtu.be)
- [__A Beginner-Friendly Introduction to Containers, VMs and Docker__ by Preeti Kasireddy](https://medium.com/free-code-camp/a-beginner-friendly-introduction-to-containers-vms-and-docker-79a9e3e119b)
- [__Lifecycle of Docker Container__ by Nitin AGARWAL](https://medium.com/@BeNitinAgarwal/lifecycle-of-docker-container-d2da9f85959)
- [Comparsion of container OSes](https://rancher.com/blog/2019/comparison-of-container-operating-systems/) - Good article on choosing the  different base OS, and the difference between busybox vs alpine
